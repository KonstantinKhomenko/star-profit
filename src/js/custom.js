$(document).ready(function(){

	
	/*     loader    */

	$(window).load(function() {

		$(".loader_inner").fadeOut();
		$(".loader").delay(400).fadeOut("slow");



	}); // load

	var $window = $(window);
	var headerToTop;

	// menu button


	$(".toggle-button").click(function(){

		$(".menu").toggleClass("active-menu");
	 	$(".sandwich").toggleClass("active");
	});


	$(".menu li a").each(function() {
		if ($(this).next().length > 0) {
			$(this).addClass("parent");
		};
	})


	$(".menu li a.parent").click(function(e){
		e.preventDefault();
		$(this).parent("li").toggleClass("hover");
	});




	/*   search button   */

	$(".header__search-icon").click(function(){
		$(".header__search").toggleClass("active-field");
	});



	/*    slick slider    */

	$(".slider").slick({

	//normal options...
	infinite: true,
	slidesToShow: 1,
	slidesToScroll: 1,
	arrows: true,
	dots: true,
	autoplay: true,

	});//slick


	/*  появления результотов калькулятора стоимости на главной  */

	$("#show-result").click(function(){
		$(".calc-main-descrip").slideDown("slow");

	    if ($.trim($(this).text()) === 'ПОКАЗАТЬ ВАРИАНТЫ') {
	        $(this).text('СКРЫТЬ ВАРИАНТЫ');
	    } else {
	        $(this).text('ПОКАЗАТЬ ВАРИАНТЫ');
			$(".calc-main-descrip").slideUp("slow");        
	    }
	});
	$(".show-result-close").click(function(){
		$(".calc-main-descrip").slideUp();
	});






		makeGrid();
		collapseText();

	$window.resize(function(){
		
		makeGrid();
		collapseText();

	}); //resize


	/*  прилипание шапки  */

	
	var headerToTop = $(".header__body").offset().top;

	$window.scroll(function(){

		if( $window.scrollTop() >= headerToTop){
			$(".header__body").addClass("fixed-head");
		}else{
			$(".header__body").removeClass("fixed-head");

		};
	});


	/*   модальные окна   */

	$(".log-in").click(function(){
		$(".underlay").addClass("active-underlay");
		$(".log-modal").css("display","block");
	});

	$(".log-modal__close").click(function(){
		$(".underlay").removeClass("active-underlay");

	});

	$(".log-modal__reg").click(function(){

		$(".log-modal").css("display","none");
		$(".reg-modal").css("display","block");
	});

		$(".reg-modal__close").click(function(){
		$(".underlay").removeClass("active-underlay");
		$(".reg-modal").css("display","none");

	});

	$(document).mouseup(function (e){
			var modal = $(".modal_close");
			var underlay = $(".underlay");
			if (!modal.is(e.target)
			    && modal.has(e.target).length === 0) {
				modal.hide();
				underlay.removeClass("active-underlay");
			}
		});


	/*   collapse text   */

	function collapseText(){
		if(($window).width() <= 480 ){
			$("#main-page-collapse").addClass("collapse");
			$('[href="#main-page-collapse"]').css("display", "inline-block");

			$("#catalog-collapse").addClass("collapse");
			$('[href="#catalog-collapse"]').css("display", "inline-block");

			$("#star-sky-collapse").addClass("collapse");
			$('[href="#star-sky-collapse"]').css("display", "inline-block");

		}else{
			$("#main-page-collapse").removeClass("collapse");
			$('[href="#main-page-collapse"]').css("display", "none");

			$("#catalog-collapse").removeClass("collapse");
			$('[href="#catalog-collapse"]').css("display", "none");

			$("#star-sky-collapse").removeClass("collapse");
			$('[href="#star-sky-collapse"]').css("display", "none");			
		}

	};

	$(".collapse_button").click(function(){
    
	    if ($.trim($(this).text()) === 'далее...') {
	        $(this).text('скрыть текст');
	    } else {
	        $(this).text('далее...');        
	    }

	});




	/*  изменяю сетку бутстрапа на разрешении меньше 768рх   */

function makeGrid(){

		if(($window).width() < 580 ){

		$(".article").removeClass("col-xs-6").addClass("col-xs-12");
		
		}else{

		$(".article").removeClass("col-xs-12").addClass("col-xs-6");	

		}

		if(($window).width() < 500 ){

		$(".catalog__item").removeClass("col-xs-6").addClass("col-xs-12");
		$(".footer__item").removeClass("col-xs-6").addClass("col-xs-12");

		}else{

		$(".catalog__item").removeClass("col-xs-12").addClass("col-xs-6");	
		$(".footer__item").removeClass("col-xs-12").addClass("col-xs-6");	

		}


		if(($window).width() < 430 ){

		// $(".calc-main__input").removeClass("col-xs-6").addClass("col-xs-12");
		$(".form-descrip__img").removeClass("col-xs-6").addClass("col-xs-12");
		$(".form-descrip__text").removeClass("col-xs-6").addClass("col-xs-12");

	}else{

		$(".calc-main__input").removeClass("col-xs-12").addClass("col-xs-6");
		$(".form-descrip__img").removeClass("col-xs-12").addClass("col-xs-6");	
		$(".form-descrip__text").removeClass("col-xs-12").addClass("col-xs-6");	
	}

}; //makeGrid


	$(".send-order__day").click(function(){
		$(".send-order__day").removeClass("day_active");
		$(this).addClass("day_active");
	});

}); // ready()




